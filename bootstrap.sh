#!/bin/bash
echo "Update system"
    sudo apt-get update -y
echo "Installing git"
    sudo apt-get install git -y
echo "Create Folder for node_modules in home directory"
    sudo mkdir /home/vagrant/node_modules
echo "Create Symlink for node_modules in shared folder"
    sudo ln -s /home/vagrant/node_modules/ node_modules
echo "Install nodejs"
    sudo apt-get install nodejs -y
    sudo ln -s /usr/bin/nodejs /usr/bin/node
echo "Install npm"
    sudo apt-get install npm -y
echo "Install sass over npm"
	sudo npm install node-sass
echo "Create public directories for web components"
    sudo mkdir -p public/css public/js public/fonts public/img
    cd public
echo "Install bower global over npm"
    sudo npm -g install bower    
echo "Install jQuery"
    sudo bower install jQuery --allow-root
echo "Install bootstrap"
	sudo bower install bootstrap --allow-root
echo "Install Font-Awesome"
    sudo bower install font-awesome --allow-root